Changelog
==========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Unreleased
**********

1.1.2
*****
* Added topics `sb.lifecycle.started`, `sb.lifecycle.finished.succeeded` and `sb.lifecycle.finished.failed` to SSE topic list

1.1.1
*****
* Added topic `script.announce` to SSE topic list