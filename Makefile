#
# CAR_OCI_REGISTRY_HOST, CAR_OCI_REGISTRY_USERNAME and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST (=artefact.skao.int) and overwrites
# CAR_OCI_REGISTRY_USERNAME and PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tango-examples/powersupply
#
CAR_OCI_REGISTRY_HOST ?= artefact.skao.int
CAR_OCI_REGISTRY_USERNAME ?= ska-telescope
PROJECT_NAME = ska-oso-oet-client

# include makefile to pick up the standard Make targets from the submodule
-include .make/base.mk
-include .make/python.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

# Set python-test make target to run unit tests and not the integration tests
PYTHON_TEST_FILE = tests/unit/

# unset defaults so settings in pyproject.toml take effect
PYTHON_SWITCHES_FOR_BLACK =
PYTHON_SWITCHES_FOR_ISORT =
PYTHON_LINE_LENGTH = 88

# Pylint tweaks:
# - C = disable msgs about Python conventions
# - R = disable msgs about refactoring
# - W0511(fixme) - these are TODO messages for future improvements
PYTHON_SWITCHES_FOR_PYLINT = --disable=C,R,fixme
