Observation Execution Tool Client
==================================

The Observation Execution Tool provides high-level scripting facilities and a high-level
scripting UI for the SKA. This project provides a REST client for the OET, which is accessible as a CLI. 
It is published as a separate library so the CLI can be used without having the large backend dependencies.

To clone this repository, run

```
git clone --recurse-submodules git@gitlab.com:ska-telescope/oso/ska-oso-oet-client.git
```

To refresh the GitLab Submodule, execute below commands:

```
git submodule update --recursive --remote
git submodule update --init --recursive
```

## Build and test

Install dependencies with Poetry and activate the virtual environment

```
poetry install
poetry shell
```


Execute the test suite and lint the project with:

```
make python-test
make python-lint
```

### Using the CLI

Once the dependencies are installed, the client should be available as a CLI. The URI for a running instance of the OET
needs to be configured via the environment variable `OET_URL`. For example, the default installation of ska-oso-oet
to the ska-oso-oet namespace in minikube will have the URI below:

```
export OET_URL=http://<KUBE_HOST>/<KUBE_NAMESPACE>/oet/api/v5/procedures
```

The CLI can then be used to run commands like `oet list`. For detailed instructions on the usage of the CLI, 
see the [OET CLI documentation](https://developer.skao.int/projects/ska-oso-oet/en/latest/cli.html#.

Note: the CLI will be running as a Python process on the host machine and access the OET inside the kubernetes cluster, 
meaning Ingress must be enabled in ska-oso-oet.




## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-oso-oet/badge/?version=latest)](https://developer.skao.int/projects/ska-oso-oet/en/latest/?badge=latest)

Documentation can be found in main OET project.