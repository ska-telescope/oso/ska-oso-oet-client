# Relax pylint. We deliberately catch all exceptions in the CLI in order to
# return a user-friendly message and rename the describe and list commands'
# input parameters for usability
#
# pylint: disable=broad-except, arguments-renamed
"""
Command-line client for the OET REST Activity Service.

This program is used to control and monitor execution of activities specified
within a Scheduling Block. It can be used to request an activity of a given SB
to be run, and to list all requested activities and their current states.
"""
import dataclasses
import datetime
import logging
import operator
from http import HTTPStatus
from typing import Dict, List, Optional

import requests
import tabulate

from ska_oso_oet_client.restclient import RestClientTemplate
from ska_oso_oet_client.sseclientwrapper import SSEClientWrapper
from ska_oso_oet_client.utils import format_error

LOGGER = logging.getLogger(__name__)

# TODO: determine reasonable timeouts per operation
# In the meantime, disable all timeouts.
TIMEOUT = None


@dataclasses.dataclass
class ActivitySummary:
    """
    Struct to hold Activity metadata. No business logic is held in this
    class.
    """

    id: int
    uri: str
    procedure_id: int
    activity_name: str
    sbd_id: str
    prepare_only: bool
    script_args: dict
    activity_states: list[tuple[str, float]]
    state: str

    @staticmethod
    def from_json(activity_json: dict):
        """
        Convert a Activity JSON payload to a ActivitySummary object

        :param activity_json: payload to convert
        :return: equivalent ActivitySummary instance
        """
        uid = activity_json["uri"].split("/")[-1]
        return ActivitySummary(
            id=uid,
            uri=activity_json["uri"],
            procedure_id=activity_json["procedure_id"],
            activity_name=activity_json["activity_name"],
            sbd_id=activity_json["sbd_id"],
            prepare_only=activity_json["prepare_only"],
            script_args=activity_json["script_args"],
            activity_states=activity_json["activity_states"],
            state=activity_json["state"],
        )


class ActivityUI(RestClientTemplate):
    """
    Command-line client for the OET REST Activity Service.

    This program is used to control and monitor execution of activities specified
    within a Scheduling Block. It can be used to request an activity of a given SB
    to be run, and to list all requested activities and their current states.
    """

    def __init__(self, server_url):
        super().__init__()
        self._client = ActivityAdapter(server_url)
        self._sseclient = SSEClientWrapper(server_url.replace("activities", "stream"))

    def run(
        self,
        activity_name: str,
        sbd_id: str,
        prepare_only: bool = False,
        script_args: Dict = None,
        create_env: bool = False,
        listen: bool = True,
    ) -> str:
        """
        Run a Scheduling Block activity.

        Each scheduling block specifies a list of activities, and for each activity details on the script
        used to run the activity. This command will prepare and run the script specified by the given SB ID
        and activity name.

        The user has the option to only prepare and not run the script relating to the activity by setting
        the prepare_only flag to True. This will load the script in the server but will not execute it. To
        execute the script the user should use the `oet procedure` commands to start the given script.

        Example for running an activity from SB with ID sbd-0001:

            oet activity run allocate sbd-0001

        Example for running an activity from SB with additional arguments:

            oet activity run allocate sbd-0001 --script_args={'main': {'kwargs': {'subarray_id': 1}}}

        :param activity_name: activity to run on the SB
        :param sbd_id: ID of the Scheduling Block to run the activity from
        :param prepare_only: Set to True if the script referred to by the activity should only
            be loaded but not yet run
        :param script_args: any arguments that should be added or modified compared to the arguments
            specified for the activity in the current state of the Scheduling Block
        :param create_env: Set to True if the activity runs a script from Git and the OET should create a Python environment
            for this script rather than using the default environment
        :param listen: display events (default=True)
        :return: Table entry for requested activity.
        """
        try:
            if listen:
                listener = self._sseclient.listen()
            activity = self._client.run(
                activity_name,
                sbd_id,
                prepare_only=prepare_only,
                script_args=script_args,
                create_env=create_env,
            )

            for line in self._tabulate([activity]).splitlines(keepends=False):
                yield line

            if listen:
                yield ""
                yield "Events"
                yield "------"
                yield ""

                for msg in listener:
                    yield msg
        except Exception as err:
            LOGGER.warning("received exception %s", err)
            yield format_error(str(err))

    @staticmethod
    def _tabulate(activities: List[ActivitySummary]) -> str:
        table_rows = [
            (
                a.id,
                a.activity_name,
                a.sbd_id,
                datetime.datetime.fromtimestamp(
                    a.activity_states[0][1], tz=datetime.timezone.utc
                ).strftime("%Y-%m-%d %H:%M:%S"),
                a.procedure_id,
                a.state,
            )
            for a in activities
        ]

        headers = ["ID", "Activity", "SB ID", "Creation Time", "Procedure ID", "State"]
        table_sections = tabulate.tabulate(table_rows, headers)
        if activities:
            table_sections = (
                table_sections
                + "\n For details on activity:- oet activity describe --aid=<ID>"
                + "\n For details on script execution:- oet procedure describe"
                " --pid=<Procedure ID>"
            )
        return table_sections

    @staticmethod
    def _tabulate_for_describe(activity: ActivitySummary) -> str:
        table_row_title = [(
            activity.id,
            activity.activity_name,
            activity.sbd_id,
            activity.procedure_id,
            activity.state,
        )]
        headers_title = ["ID", "Activity", "SB ID", "Procedure ID", "State"]

        table_row_pid = [(activity.uri, activity.prepare_only)]
        headers_pid = ["URI", "Prepare Only"]
        table_rows_states = [
            (
                datetime.datetime.fromtimestamp(
                    s[1], tz=datetime.timezone.utc
                ).strftime("%Y-%m-%d %H:%M:%S.%f"),
                s[0],
            )
            for s in activity.activity_states
        ]

        table_rows_states.sort(key=operator.itemgetter(0))
        headers_states = ["Time", "State"]

        # define default table sections...
        table_sections = [
            tabulate.tabulate(table_row_title, headers_title),
            tabulate.tabulate(table_row_pid, headers_pid),
            tabulate.tabulate(table_rows_states, headers_states),
        ]

        if activity.script_args:
            table_rows_args = [
                (
                    fn,
                    activity.script_args[fn]["args"],
                    activity.script_args[fn]["kwargs"],
                )
                for fn in activity.script_args
            ]

            headers_args = ["Method", "Arguments", "Keyword Arguments"]

            table_sections.append("\nScript Arguments \n-----------------------")
            table_sections.append(tabulate.tabulate(table_rows_args, headers_args))
        table_sections.append(
            "\nFor details on script execution related to the activity \n"
            "use `oet procedure` commands with the Procedure ID"
        )

        return "\n\n".join(table_sections)

    def list(self, aid=None) -> str:
        """
        List all requested activities.

        This command has an optional arguments: a numeric activity ID to list.
        If no ID is specified, all procedures will be listed.

        :param aid: (optional) IDs of item to list
        :return: Table entries for requested item(s)
        """
        return super().list(aid)

    def describe(self, aid=None) -> str:
        """
        Display information on an activity.

        This will display the details of a specific activity, including
        state history. If no ID is declared, the last activity to be
        created will be described.

        :param aid: ID of the activity to describe
        """
        return super().describe(aid)


class ActivityAdapter:
    """A simple CLI REST client using python-fire for the option parsing"""

    def __init__(self, server_url):
        """

        :param server_url: URI of target REST server for activities
        """
        self.server_url = server_url

    def list(self, aid: Optional[int] = None) -> List[ActivitySummary]:
        """
        List activities known to the OET.

        This command accepts an optional numeric activity ID. If no ID is
        specified, all procedures will be listed.

        :param aid: (optional) ID of activity to list
        :return: List of ActivitySummary instances
        """
        if aid is not None:
            url = f"{self.server_url}/{aid}"
            response = requests.get(url, timeout=TIMEOUT)
            if response.status_code == HTTPStatus.OK:
                procedure_json = response.json()["activity"]

                return [ActivitySummary.from_json(procedure_json)]
            else:
                raise RuntimeError(response.text)

        url = self.server_url
        response = requests.get(url, timeout=TIMEOUT)
        procedures_json = response.json()["activities"]
        return [ActivitySummary.from_json(d) for d in procedures_json]

    def run(
        self,
        activity_name: str,
        sbd_id: str,
        prepare_only: bool = False,
        script_args: Dict = None,
        create_env: bool = False,
    ) -> ActivitySummary:
        """
        Prepare and run the script referred to by scheduling block ID and activity name.

        Set prepare_only to True if script should only be loaded and not run.

        :param activity_name: activity to run on the SB
        :param sbd_id: ID of the Scheduling Block to run the activity from
        :param prepare_only: Set to True if the script referred to by the activity should only
            be loaded but not yet run
        :param script_args: any arguments that should be added or modified compared to the arguments
            specified for the activity in the current state of the Scheduling Block
        :param create_env: Set to True if the activity runs a script from Git and the OET should create a Python environment
            for this script rather than using the default environment
        :return: Summary of created activity.
        """
        if script_args is None:
            script_args = dict()
        request_json = {
            "sbd_id": sbd_id,
            "activity_name": activity_name,
            "prepare_only": prepare_only,
            "script_args": script_args,
            "create_env": create_env,
        }
        LOGGER.debug("Create payload: %s", request_json)

        response = requests.post(self.server_url, json=request_json, timeout=TIMEOUT)
        response_as_dict = response.json()
        if response.status_code == HTTPStatus.CREATED:
            return ActivitySummary.from_json(response_as_dict["activity"])
        raise RuntimeError(response.text)
