"""
Command-line client for the OET REST Service.

This program is used to control and monitor a remote OET script executor.
This tool provides a client interface for activity and procedure lead
script execution, see `oet activity --help` and `oet procedure --help`
for more details.
"""

import logging
from os import getenv
from typing import Generator, Optional

import fire

from ska_oso_oet_client.activityclient import ActivityUI
from ska_oso_oet_client.procedureclient import ProcedureUI
from ska_oso_oet_client.sseclientwrapper import SSEClientWrapper

LOGGER = logging.getLogger(__name__)

KUBE_NAMESPACE = getenv("KUBE_NAMESPACE", "ska-oso-oet")


#
# Monkey patch the Fire flag handling: Fire uses flags for arguments which should be
# passed to the underlying function, and 'Fire flags' that are used by Fire internally (eg --help) which
# are expected after `--` in the CLI call.
# For functions without kwargs this doesn't seem to cause an issue, but if kwargs are present in the function
# signature then --help is converted to a boolean and passed to the function.
# For example, `oet start --help` would pass `help=True` to the function, but `oet start -- --help`
# would show the docstring help.
# The latter is not intuitive for the user, so this monkey patch will always treat --help as a Fire flag
#
# Taken from issue:
# https://github.com/google/python-fire/issues/258
def _SeparateFlagArgs(args):
    # Original functionality in case user does pass `--`
    if "--" in args:
        separator_index = len(args) - 1 - args[::-1].index("--")  # index of last --
        flag_args = args[separator_index + 1 :]
        args = args[:separator_index]
        return args, flag_args

    # If not, treat --help as special case
    try:
        index = args.index("--help")
        args = args[:index]
        return args, ["--help"]
    except ValueError:
        return args, []


fire.core.parser.SeparateFlagArgs = _SeparateFlagArgs


class RestClientUI:
    """
    OET command-line interface.

    This program is used to control and monitor a remote OET script executor.
    This tool provides a client interface for activity and procedure lead
    script execution, see `oet activity --help` and `oet procedure --help`
    for more details.
    """

    def __init__(self, server_url=None):
        """
        Create a new client for the OET script execution service.

        By default, the client will attempt to connect to a server at
        localhost.

        :param server_url: URI of the target REST server
        """
        if server_url is None:
            server_url = getenv(
                "OET_URL",
                f"http://ska-oso-oet-rest-test:5000/{KUBE_NAMESPACE}/oet/api/v5",
            )

        self.activity = ActivityUI(server_url + "/activities")
        self.procedure = ProcedureUI(server_url + "/procedures")

        self._sse_client = SSEClientWrapper(server_url + "/stream")

    def listen(
        self,
        topics: Optional[str] = "all",
        exclude: Optional[str] = "request,procedure.pool",
    ) -> Generator[str, None, None]:
        """
        Display OET events.

        This command will display all events emitted by the OET and user scripts
        that meet the topic filter criteria set by the --topics and --exclude
        command line arguments.

        The stop displaying events, press CTRL+C.

        :param topics: event topics to display, or 'all' for all (default='all')
        :param exclude: event topics to exclude (default='request,procedure.pool')
        """
        return self._sse_client.listen(topics, exclude)


def main():
    """
    Fire entry function to provide a CLI interface for REST client.
    """
    fire.Fire(RestClientUI)


# This statement is included so that we can run this module and test the REST
# client directly without installing the OET project
if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    main()
