# Relax pylint. We deliberately catch all exceptions in the CLI in order to
# return a user-friendly message and rename the describe and list commands'
# input parameters for usability
#
# pylint: disable=broad-except, arguments-renamed
"""
Command-line client for the OET REST Procedures Service.

This program is used to control and monitor a remote OET script executor. It can
be used to request 'procedure creation', which loads a Python script and
prepares it for execution; to 'start a procedure', which starts execution of a
script prepared in a prior 'create procedure' call, and to list all prepared
and running procedures held in the remote server.
"""
import dataclasses
import datetime
import itertools as IT
import logging
import operator
from http import HTTPStatus
from typing import Dict, Generator, List, Optional

import requests
import tabulate

from ska_oso_oet_client.restclient import RestClientTemplate
from ska_oso_oet_client.sseclientwrapper import SSEClientWrapper
from ska_oso_oet_client.utils import format_error

LOGGER = logging.getLogger(__name__)

# TODO: determine reasonable timeouts for each operation, e.g., (3.05, 60).
# In the meantime, disable all timeouts.
TIMEOUT = None


@dataclasses.dataclass
class ProcedureSummary:
    """
    Struct to hold Procedure metadata. No business logic is held in this
    class.
    """

    id: int
    uri: str
    script_args: dict
    script: dict
    history: dict
    state: str

    @staticmethod
    def from_json(procedure_json: dict):
        """
        Convert a Procedure JSON payload to a ProcedureSummary object

        :param procedure_json: payload to convert
        :return: equivalent ProcedureSummary instance
        """
        uid = procedure_json["uri"].split("/")[-1]
        return ProcedureSummary(
            id=uid,
            uri=procedure_json["uri"],
            script_args=procedure_json["script_args"],
            script=procedure_json["script"],
            history=procedure_json["history"],
            state=procedure_json["state"],
        )


class ProcedureUI(RestClientTemplate):
    """
    OET command-line interface.

    This program is used to control and monitor a remote OET script executor.
    Using this tool, you can instruct the remote OET backend to load a Python
    script, call a function of that script, and abort script execution. This
    tool can also report a recent history of script execution and monitor the
    events published by the OET backend and running scripts.

    Note that multiple scripts may be loaded and prepared for execution, but
    only one script can run at a time.
    """

    def __init__(self, server_url):
        super().__init__()
        self._client = ProcedureAdapter(server_url)
        self._sseclient = SSEClientWrapper(server_url.replace("procedures", "stream"))

    @staticmethod
    def _tabulate(procedures: List[ProcedureSummary]) -> str:
        table_rows = [
            (
                p.id,
                p.script["script_uri"],
                datetime.datetime.fromtimestamp(
                    p.history["process_states"][0][1], tz=datetime.timezone.utc
                ).strftime("%Y-%m-%d %H:%M:%S"),
                p.state,
            )
            for p in procedures
        ]

        headers = ["ID", "Script", "Creation Time", "State"]
        table_sections = tabulate.tabulate(table_rows, headers)
        if procedures:
            table_sections = (
                table_sections
                + "\n For more details:- oet procedure describe --pid=<id>"
            )
        return table_sections

    @staticmethod
    def _tabulate_for_describe(procedure: ProcedureSummary) -> str:
        table_row_title = [
            (procedure.id, procedure.script["script_uri"], procedure.uri)
        ]
        headers_title = ["ID", "Script", "URI"]

        table_rows_args = [
            (
                index,
                s,
                procedure.script_args[s]["args"],
                procedure.script_args[s]["kwargs"],
            )
            for index, s in enumerate(procedure.script_args, start=1)
        ]

        headers_args = ["Index", "Method", "Arguments", "Keyword Arguments"]
        counter = IT.count(1)
        table_rows_states = [
            (
                datetime.datetime.fromtimestamp(
                    s[1], tz=datetime.timezone.utc
                ).strftime("%Y-%m-%d %H:%M:%S.%f"),
                f"{s[0]} {next(counter)}" if s[0] == "RUNNING" else s[0],
            )
            for s in procedure.history["process_states"]
        ]

        table_rows_states.sort(key=operator.itemgetter(0))
        headers_states = ["Time", "State"]

        # define default table sections...
        table_sections = [
            tabulate.tabulate(table_row_title, headers_title),
            tabulate.tabulate(table_rows_states, headers_states),
            tabulate.tabulate(table_rows_args, headers_args),
        ]

        if "git_args" in procedure.script:
            table_row_git = [(
                procedure.script["git_args"]["git_repo"],
                procedure.script["git_args"]["git_branch"],
                procedure.script["git_args"]["git_commit"],
            )]

            table_row_git.sort(key=operator.itemgetter(0))
            headers_git = ["Repository", "Branch", "Commit"]
            table_sections.append(tabulate.tabulate(table_row_git, headers_git))

        # .. and add stacktrace if present
        stacktrace = procedure.history["stacktrace"]
        if stacktrace:
            table_sections.append(f"Stack Trace:\n------------\n{stacktrace}")

        return "\n\n".join(table_sections)

    def create(
        self,
        script_uri: str,
        *args,
        subarray_id=1,
        git_repo: str = None,
        git_branch: str = None,
        git_commit: str = None,
        create_env: bool = False,
        **kwargs,
    ) -> str:
        """
        Prepare a Procedure (=prepare to run a script).

        This command tells the OET backend to prepare to run a user script.
        The OET backend will load the requested script, prepare the Python
        environment if requested, and call the script's init function if
        present.

        The OET can load scripts from the container filesystem and from remote
        git repositories.

        The user script URI should begin with either file:// to reference a
        script that exists within the OET backend's filesystem / default
        environment, or git:// to give the relative path of a script within
        a git project.

        The OET has some default behaviour regarding non-default scripts
        such as git scripts that can be modified through use of the --git_repo,
        --git_branch, --git_commit, and --create_env command line arguments.

        1. --git_repo: By default, the OET assumes the referenced script
           belongs to the ska-oso-scripting gitlab project, and will retrieve
           the project and attempt to find the file accordingly. To point to
           a different git repository, define the --git_repo argument.

        2. --git_branch: By default, the OET retrieves the default git project
           for the specified git project (usually main or master). Defining
           the --git_branch argument will cause the specified git branch to be
           retrieved.

        3. --git-commit: By default, the OET will use the latest commit for
           the target git branch. A different commit can be specified by
           setting the  --git_commit argument to a git commit hash. Note that
           this will override any --git_branch setting.

        4. --create_env: By default, the script will execute using the
           default Python environment of the OET backend container. If the
           script has dependencies not met by the default environment, setting
           --create_env=True will instruct the OET to create a new Python
           environment and install the project's dependencies, as specified
           by the project's Poetry configuration or requirements.txt. If a
           suitable environment already exists on the backend, as denoted by
           matching git commit hash for the create request, the existing
           environment will be reused.

        Arguments will be passed to the Procedure's init function. Git
        arguments should only be provided if script_uri prefix is git://

        Example for running procedure from filesystem:

            oet procedure create file:///scripts/observe.py subarray_id=2 --verbose=true

        Example for running procedure from git:

            oet procedure create git://relative/path/to/script.py --git_repo=http://gitlab.com/repo-name --create_env=False

        :param script_uri: script URI, e.g., file:///test.py
        :param args: script positional arguments
        :param subarray_id: Sub-array controlled by this OET instance
        :param git_repo: Path to git repository
            (default=http://gitlab.com/ska-telescope/oso/ska-oso-scripting)
        :param git_branch: Branch within the git repository, defaults to master if not provided
        :param git_commit: Git commit hash, defaults to latest commit on the given branch.
            Branch does not need to be specified if commit hash is provided
        :param create_env: Install dependencies from the procedure source project. Set to False by default.
        :param kwargs: script keyword arguments
        :return: Table entry for created procedure.
        """

        # Iterating over the Python kwargs dictionary
        git_args = dict()
        init_kwargs = dict()
        init_kwargs["subarray_id"] = subarray_id

        for arg in kwargs.keys():
            init_kwargs[arg] = kwargs[arg]

        if git_repo:
            git_args["git_repo"] = git_repo
        if git_branch:
            git_args["git_branch"] = git_branch
        if git_commit:
            git_args["git_commit"] = git_commit

        init_args = dict(args=args, kwargs=init_kwargs)
        try:
            procedure = self._client.create(
                script_uri,
                init_args=init_args,
                git_args=git_args,
                create_env=create_env,
            )
        except Exception as err:
            LOGGER.debug("received exception %s", err)
            return format_error(str(err))

        return self._tabulate([procedure])

    def start(
        self, *args, pid=None, listen=True, **kwargs
    ) -> Generator[str, None, None]:
        """
        Run a Procedure.

        This will start the procedure with the specified ID. If no procedure
        ID is declared, the most recent procedure to be created will be
        started.

        By default, this interface will run the requested command and then
        immediately start listening to the OET backend's event stream so that
        events and messages emitted by the backend and user script are seen.
        To stop listening to events, press CTRL+C. Add the --listen=False
        argument to the command to run the command silent, with no connection
        to the event stream.

        This command instructs the OET to run the main() function of the
        target script. Arguments provided on the command line will be passed
        as positional and keyword arguments to the main() function.

        Examples:

            # calls main() of the last created script, passing the SBI ID to
            # the function. Equivalent to main('sbi-mvp01-20200325-00001')
            oet procedure start sbi-mvp01-20200325-00001

            # calls main() of the script PID #3, passing the positional argument
            # and keyword arguments to the script. Equivalent to calling
            # main('hello', foo='bar')
            oet procedure start --pid=3 'hello' --foo=bar

        :param pid: ID of the procedure to start
        :param listen: display events (default=True)
        :param args: late-binding position arguments for script
        :param kwargs: late-binding kwargs for script
        :return: Table entry for running procedure
        """
        if pid is None:
            procedures = self._client.list()
            if not procedures:
                yield "No procedures to start"
                return

            procedure = procedures[-1]
            if procedure.state != "READY":
                yield (
                    f"The last procedure created is in {procedures[-1].state} state "
                    "and cannot be started, please specify a valid procedure ID."
                )
                return
            pid = procedure.id
        else:
            procedure = self._client.list(pid)[0]
            if procedure.state != "READY":
                yield (f"Cannot start a procedure in state {procedure.state}.")
                return

        run_args = dict(args=args, kwargs=kwargs)
        try:
            if listen:
                listener = self._sseclient.listen()

            procedure = self._client.start(pid, run_args=run_args)
            for line in self._tabulate([procedure]).splitlines(keepends=False):
                yield line

            if listen:
                yield ""
                yield "Events"
                yield "------"
                yield ""

                for msg in listener:
                    yield msg

        except Exception as err:
            LOGGER.debug("received exception %s", err)
            yield format_error(str(err))

    def stop(self, pid=None, run_abort=True) -> str:
        """
        Terminate execution.

        This will stop the execution of a currently running procedure
        with the specified ID.If no procedure ID is declared, the first
        procedure with running status will be stopped.

        :param pid: ID of the procedure to stop
        :param run_abort: If True (default), executes abort script once running
            script has terminated
        :return: Empty table entry
        """
        if pid is None:
            running_procedures = [
                p for p in self._client.list() if p.state == "RUNNING"
            ]
            if not running_procedures:
                return "No procedures to stop"
            if len(running_procedures) > 1:
                return (
                    "WARNING: More than one procedure is running. "
                    "Specify ID of the procedure to stop."
                )
            pid = running_procedures[0].id
        try:
            response = self._client.stop(pid, run_abort)
            return response
        except Exception as err:
            LOGGER.debug("received exception %s", err)
            return format_error(str(err))

    def describe(self, pid=None) -> str:
        """
        Display Procedure information.

        This will display the state history of a specified procedure,
        including the stack trace is the procedure failed. If no procedure ID
        is declared, the last procedure to be created will be described.

        :param pid: ID of procedure to describe
        """
        return super().describe(pid)

    def list(self, pid=None) -> str:
        """
        List the state of current and recently run scripts.

        This command has an optional arguments: a numeric procedure ID to list.
        If no ID is specified, all procedures will be listed.

        :param pid: (optional) IDs of item to list
        :return: Table entries for requested item(s)
        """
        return super().list(pid)


class ProcedureAdapter:
    """A simple CLI REST client using python-fire for the option parsing"""

    def __init__(self, server_url):
        """
        Create a new OET REST adapter.

        :param server_url: URI of target REST server
        """
        self.server_url = server_url

    def list(self, pid: Optional[int] = None) -> List[ProcedureSummary]:
        """
        List procedures known to the OET.

        This command accepts an optional numeric procedure ID. If no ID is
        specified, all procedures will be listed.

        :param pid: (optional) ID of procedure to list
        :return: List of ProcedureSummary instances
        """
        if pid is not None:
            url = f"{self.server_url}/{pid}"
            response = requests.get(url, timeout=TIMEOUT)
            if response.status_code == HTTPStatus.OK:
                procedure_json = response.json()["procedure"]
                return [ProcedureSummary.from_json(procedure_json)]
            else:
                raise RuntimeError(response.text)

        url = self.server_url
        response = requests.get(url, timeout=TIMEOUT)
        procedures_json = response.json()["procedures"]
        return [ProcedureSummary.from_json(d) for d in procedures_json]

    def create(
        self,
        script_uri: str,
        init_args: Dict = None,
        git_args: Dict = None,
        create_env: bool = False,
    ) -> ProcedureSummary:
        """
        Create a new Procedure.

        Arguments given in init_args will be passed to the Procedure's
        init function. The init_args argument should be a dict with 'args' and
        'kwargs' entries for positional and named arguments respectively,
        e.g.,

            init_args={args=[1,2,3], kwargs=dict(kw1=2, kw3='abc')}

        Argument given in git_args should be a dict e.g.,
             git_args={"git_repo": "git://foo.git","git_branch": "main","git_commit": "HEAD"}

        :param script_uri: script URI, e.g., file://test.py or git://test.git
        :param init_args: script initialisation arguments
        :param git_args: git script arguments
        :param create_env: Install dependencies from the procedure source project. Set to False by default.
        :return: Summary of created procedure.
        """
        if not (script_uri.startswith("file://") or script_uri.startswith("git://")):
            raise ValueError(
                f"Script URI type not handled: {script_uri.split('//')[0]}"
            )

        script = dict(script_type="filesystem", script_uri=script_uri)
        if init_args is None:
            init_args = dict(args=[], kwargs={})

        if git_args and "file://" in script_uri:
            raise ValueError(
                f"Invalid request, Git arguments: {git_args} are not required for"
                " Filesystem script."
            )
        if "git://" in script_uri:
            script = dict(
                script_type="git",
                script_uri=script_uri,
                git_args=git_args,
                create_env=create_env,
            )

        request_json = {
            "script_args": {
                "init": init_args,
            },
            "script": script,
        }
        LOGGER.debug("Create payload: %s", request_json)

        response = requests.post(self.server_url, json=request_json, timeout=TIMEOUT)
        response_as_dict = response.json()
        if response.status_code == HTTPStatus.CREATED:
            return ProcedureSummary.from_json(response_as_dict["procedure"])
        raise RuntimeError(response.text)

    def start(self, pid, run_args=None) -> ProcedureSummary:
        """
        Start the specified Procedure.

        Arguments given in run_args will be passed to the Procedure
        entry method. The run_args argument should be a dict with 'args' and
        'kwargs' entries for positional and named arguments respectively,
        e.g.,

            run_args={args=[1,2,3], kwargs=dict(kw1=2, kw3='abc')}

        :param pid: ID of script to execute
        :param run_args: late-binding script arguments
        :return: Summary of running procedure.
        """
        url = f"{self.server_url}/{pid}"

        if run_args is None:
            run_args = dict(args=[], kwargs={})

        request_json = {"script_args": {"main": run_args}, "state": "RUNNING"}
        LOGGER.debug("Start payload: %s", request_json)

        response = requests.put(url, json=request_json, timeout=TIMEOUT)
        response_as_dict = response.json()
        if response.status_code == HTTPStatus.OK:
            return ProcedureSummary.from_json(response_as_dict["procedure"])
        raise RuntimeError(response.text)

    def stop(self, pid, run_abort=True):
        """
        Stop the specified Procedure.

        :param pid: ID of script to stop
        :param run_abort: If True (default), executes abort script once running
            script has terminated
        :return: success/failure message
        """
        url = f"{self.server_url}/{pid}"

        request_json = {"abort": run_abort, "state": "STOPPED"}
        LOGGER.debug("Stop payload: %s", request_json)

        response = requests.put(url, json=request_json, timeout=TIMEOUT)
        response_as_dict = response.json()
        if response.status_code == HTTPStatus.OK:
            return response_as_dict["abort_message"]
        raise RuntimeError(response.text)
