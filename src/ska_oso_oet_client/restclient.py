# Relax pylint. We deliberately catch all exceptions in the CLI in order to
# return a user-friendly message.
#
# pylint: disable=broad-except
import logging
from typing import Any, List

from .utils import format_error

LOGGER = logging.getLogger(__name__)


class RestClientTemplate:
    """
    A template class for Fire-based CLI classes meant for listing and describing
    OET related resources. Further methods for controlling the resources should
    be implemented for each extending class.

    A private field, _client, should implement list() function which retrieves
    the requested item(s) from an external resource.

    Any extending class should implement _tabulate and _tabulate_for_describe
    functions that should return a string representation of the item(s) to
    display to the user.
    """

    def __init__(self):
        """
        Create a new client for the OET script execution service.

        By default, the client will attempt to connect to a server at
        localhost.
        """
        self._client = None

    def describe(self, item_id=None) -> str:
        """
        Display information on given resource.

        This will display information of a specified resource. If no ID
        is declared, the last item to be created will be described.

        :param item_id: ID of item to describe
        """
        try:
            if item_id is None:
                items = self._client.list()
                if not items:
                    return "No items to describe"
                item_id = items[-1].id
            item = self._client.list(item_id)
        except Exception as err:
            LOGGER.debug("received exception %s", err)
            return format_error(str(err))
        return self._tabulate_for_describe(item[0])

    def list(self, item_id=None) -> str:
        """
        List the state of current and recently run scripts.

        This command has an optional arguments: a numeric procedure ID to list.
        If no ID is specified, all procedures will be listed.

        :param item_id: (optional) IDs of item to list
        :return: Table entries for requested item(s)
        """
        try:
            procedures = self._client.list(item_id)
        except Exception as err:
            LOGGER.warning("received exception %s", err)
            return format_error(str(err))
        return self._tabulate(procedures)

    @staticmethod
    def _tabulate_for_describe(item: Any):
        raise NotImplementedError

    @staticmethod
    def _tabulate(items: List[Any]):
        raise NotImplementedError
