# Relax pylint. We deliberately catch all exceptions in the CLI in order to
# return a user-friendly message.
#
# pylint: disable=broad-except
import json
import logging
from typing import Generator, List, Optional

import sseclient

from ska_oso_oet_client.utils import format_error

LOGGER = logging.getLogger(__name__)


#
# Monkey patch SSEClient to solve issue with gzip-compressed SSE streams
#
# Patch taken verbatim from:
# https://github.com/Count-Count/sseclient/tree/dont_use_raw_reads_with_gzipped_or_chunked_streams
#
def iter_content(self):
    if (
        hasattr(self.resp.raw, "_fp")
        and hasattr(self.resp.raw._fp, "fp")  # pylint: disable=protected-access
        and hasattr(self.resp.raw._fp.fp, "read1")  # pylint: disable=protected-access
        and not self.resp.raw.chunked
        and not self.resp.raw.getheader("Content-Encoding")
    ):

        def generate():
            while True:
                chunk = self.resp.raw._fp.fp.read1(  # pylint: disable=protected-access
                    self.chunk_size
                )
                if not chunk:
                    break
                yield chunk

        return generate()

    else:
        # short reads cannot be used, this will block until
        # the full chunk size is actually read
        return self.resp.iter_content(self.chunk_size)


sseclient.SSEClient.iter_content = iter_content


class SSEClientWrapper:
    # turn off Black for these topic-to-lambda mappings
    # fmt: off
    TOPIC_DICT = {
        "request.procedure.create": (
            lambda evt: f'User request: prepare {evt["cmd"]["script"]["script_uri"]} for execution on subarray {evt["cmd"]["init_args"]["kwargs"]["subarray_id"]}'
        ),
        "request.procedure.list": (
            lambda evt: "User request to list all the procedures is received"
        ),
        "request.procedure.start": (
            lambda evt: f'User request: start execution of process #{evt["cmd"]["process_uid"]}'
        ),
        "request.procedure.stop": (
            lambda evt: f'User request: stop procedure #{evt["cmd"]["process_uid"]} with {"" if evt["cmd"]["run_abort"] else "no"} abort'
        ),
        "request.activity.run": (
            lambda evt: f'User request: Run activity {evt["cmd"]["activity_name"]} of SB {evt["cmd"]["sbd_id"]}'
        ),
        "procedure.pool.list": (
            lambda evt: "Enumerating current procedures and status"
        ),
        "procedure.lifecycle.created": (
            lambda evt: f'Procedure {evt["result"]["id"]} ({evt["result"]["script"]["script_uri"]}) ready for execution on subarray {SSEClientWrapper._extract_subarray_id_from_result(evt)}'
        ),
        "procedure.lifecycle.started": (
            lambda evt: f'Procedure {evt["result"]["id"]} ({evt["result"]["script"]["script_uri"]}) started execution on subarray {SSEClientWrapper._extract_subarray_id_from_result(evt)}'
        ),
        "procedure.lifecycle.stopped": (
            # pylint: disable=unnecessary-lambda
            lambda evt: SSEClientWrapper._extract_result_from_abort_result(evt)
        ),
        "procedure.lifecycle.failed": (
            lambda evt: f'Procedure {evt["result"]["id"]} ({evt["result"]["script"]["script_uri"]}) execution failed on subarray {SSEClientWrapper._extract_subarray_id_from_result(evt)}'
        ),
        "activity.lifecycle.running": (
            lambda evt: f'Activity {evt["result"]["id"]} ({evt["result"]["activity_name"]} of SB {evt["result"]["sbd_id"]}) is running'
        ),
        "user.script.announce": lambda evt: f'Script message: {evt["msg"]}',
        "script.announce": lambda evt: f'Script message: {evt["msg"]}',
        "sb.lifecycle.started": (
            lambda evt: f'Execution of SB {evt["sbi_id"]} started'
        ),
        "sb.lifecycle.finished.failed": (
            lambda evt: f'Execution of SB {evt["sbi_id"]} failed'
        ),
        "sb.lifecycle.finished.succeeded": (
            lambda evt: f'Execution of SB {evt["sbi_id"]} complete'
        ),
        "sb.lifecycle.allocated": (
            lambda evt: f'Resources allocated using SB {evt["sb_id"]}'
        ),
        "sb.lifecycle.observation.started": (
            lambda evt: f'Observation for SB {evt["sb_id"]} started'
        ),
        "sb.lifecycle.observation.finished.succeeded": (
            lambda evt: f'Observation for SB {evt["sb_id"]} complete'
        ),
        "sb.lifecycle.observation.finished.failed": (
            lambda evt: f'Observation for SB {evt["sb_id"]} failed'
        ),
        "subarray.resources.allocated": (
            lambda evt: f'Subarray {evt["subarray_id"]}: resources allocated'
        ),
        "subarray.resources.deallocated": (
            lambda evt: f'Subarray {evt["subarray_id"]}: resources released'
        ),
        "subarray.configured": (
            lambda evt: f'Subarray {evt["subarray_id"]} configured'
        ),
        "subarray.scan.started": (
            lambda evt: f'Subarray {evt["subarray_id"]}: scan started'
        ),
        "subarray.scan.finished": (
            lambda evt: f'Subarray {evt["subarray_id"]}: scan complete'
        ),
        "subarray.fault": (
            lambda evt: f'Subarray {evt["subarray_id"]} error: {evt["error"]}'
        ),
        "scan.lifecycle.configure.started": (
            lambda evt: f'SB {evt["sb_id"]}: configuring for scan {evt["scan_id"]}'
        ),
        "scan.lifecycle.configure.complete": (
            lambda evt: f'SB {evt["sb_id"]}: scan {evt["scan_id"]} configuration complete'
        ),
        "scan.lifecycle.configure.failed": (
            lambda evt: f'SB {evt["sb_id"]}: scan {evt["scan_id"]} configuration failed'
        ),
        "scan.lifecycle.start": (
            lambda evt: f'SB {evt["sb_id"]}: scan {evt["scan_id"]} starting'
        ),
        "scan.lifecycle.end.succeeded": (
            lambda evt: f'SB {evt["sb_id"]}: scan {evt["scan_id"]} complete'
        ),
        "scan.lifecycle.end.failed": (
            lambda evt: f'SB {evt["sb_id"]}: scan {evt["scan_id"]} failed'
        ),
    }
    # fmt: on

    def __init__(self, server_url):
        self.server_url = server_url

    @staticmethod
    def _extract_result_from_abort_result(evt: dict):
        """
        PI8 workaround.

        A script stopping naturally returns a single ProcedureSummary.
        However, an aborted script returns a _list_ of ProcedureSummaries, one
        summary for each post-abort script. If the result is a list, this
        method returns the first result found, which is enough for PI8.

        TODO refactor stop message to a common type
        """
        try:
            evt["result"] = evt["result"][0]
        except IndexError:
            # stop script but no post-abort script run
            # no other info available in message!
            return "Procedure stopped"
        except (TypeError, KeyError):
            pass
        return (
            f'Procedure {evt["result"]["id"]} ({evt["result"]["script"]["script_uri"]})'
            " execution complete on subarray"
            f" {SSEClientWrapper._extract_subarray_id_from_result(evt)}"
        )

    @staticmethod
    def _extract_subarray_id_from_result(evt: dict):
        """
        A helper function for retrieving the sub-array ID that is added as a keyword
        argument to all procedure commands made through the ProcedureUI client. Will return
        empty string if sub-array ID is not defined (e.g. in a case where the procedure is run by
        an activity which does not supply default value for sub-array ID or when a topic with
        unexpected response structure is given)
        """
        expected_topics = [
            "procedure.lifecycle.created",
            "procedure.lifecycle.stopped",
            "procedure.lifecycle.started",
        ]
        if evt.get("topic") not in expected_topics:
            return ""

        init_args = [
            args["fn_args"]
            for args in evt["result"]["script_args"]
            if args["fn"] == "init"
        ]
        return init_args[0]["kwargs"].get("subarray_id", "")

    def listen(
        self,
        topics: Optional[str] = "all",
        exclude: Optional[str] = "request,procedure.pool",
    ) -> Generator[str, None, None]:
        """
        Display OET events.

        This command will display all events emitted by the OET and user scripts
        that meet the topic filter criteria set by the --topics and --exclude
        command line arguments.

        The stop displaying events, press CTRL+C.

        :param topics: event topics to display, or 'all' for all (default='all')
        :param exclude: event topics to exclude (default='request,procedure.pool')
        """
        if topics == "all":
            topics = list(SSEClientWrapper.TOPIC_DICT.keys())
        else:
            topics = topics.split(",")

        exclude_topics = exclude.split(",")
        to_exclude = [
            t for e in exclude_topics for t in topics if e and t.startswith(e)
        ]
        topics = [t for t in topics if t not in to_exclude]

        try:
            for evt in sseclient.SSEClient(self.server_url):
                output = self._filter_event_messages(evt, topics)
                if output:
                    yield f"- {output}"
        except KeyboardInterrupt as err:
            LOGGER.debug("received exception %s", err)
        except Exception as err:
            LOGGER.debug("received exception %s", err)
            yield format_error(str(err))

    @staticmethod
    def _filter_event_messages(evt: sseclient.Event, topics: List[str]) -> str:
        if not evt.data:
            return ""

        try:
            event_dict = json.loads(evt.data)
        except json.decoder.JSONDecodeError:
            return f"ERROR Could not parse event: {evt}"

        event_topic = event_dict.get("topic", None)
        if event_topic not in topics:
            return ""

        formatter = SSEClientWrapper.TOPIC_DICT.get(event_topic, str)
        try:
            return formatter(event_dict)
        except KeyError:
            LOGGER.debug("Error parsing event: %s", event_dict)
            return ""
