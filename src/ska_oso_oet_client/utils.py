"""
Utility module for OET client. This module contains code whose usefulness is not specific to one domain
"""

import json


def format_error(error_json: str) -> str:
    try:
        error_d = json.loads(error_json)
        msg_type = error_d["type"]
        message = error_d["Message"]
        error = error_d["error"]
        msg = f"Server encountered error {error}:\n  {msg_type}: {message}"
    except ValueError:
        # ValueError raised if error is not valid JSON. This happens at least when
        # REST server is not running and returns Connection refused error
        msg = f"The server encountered a problem: {error_json}"
    return f"{msg}"
