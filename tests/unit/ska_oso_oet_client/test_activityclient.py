# pylint: disable=W1510,C0116,C0302,C0103
# - W1510(subprocess-run-check) - not an issue - this is a test
"""
Unit tests for the REST client module.
"""
from http import HTTPStatus
from unittest import mock

import fire
import requests_mock
from pytest import raises

from ska_oso_oet_client.activityclient import (
    ActivityAdapter,
    ActivitySummary,
    ActivityUI,
)

ACTIVITIES_URI = "http://localhost:5000/api/v1.0/activities"

EXAMPLE_ACTIVITY_JSON = {
    "id": 1,
    "sbd_id": "1234",
    "activity_name": "allocate",
    "procedure_id": 1,
    "activity_states": [
        ["REQUESTED", 1601303225.8232567],
        ["IDLE", 1601303225.8234567],
    ],
    "prepare_only": "false",
    "create_env": "false",
    "script_args": {},
    "state": "IDLE",
    "uri": "http://localhost:5000/api/v1.0/activities/1",
}

EXAMPLE_ACTIVITY_JSON_OPTIONAL_ARGS = {
    "id": 2,
    "sbd_id": "1234",
    "activity_name": "allocate",
    "procedure_id": 2,
    "prepare_only": "true",
    "script_args": {
        "init": {
            "args": [],
            "kwargs": {
                "subarray": 1,
            },
        },
        "main": {"args": [], "kwargs": {}},
    },
    "activity_states": [
        ["REQUESTED", 1601303225.8232567],
        ["IDLE", 1601303225.8234567],
    ],
    "state": "IDLE",
    "uri": "http://localhost:5000/api/v1.0/activities/2",
}

RUN_OR_LIST_ACTIVITY_RESPONSE = {"activity": EXAMPLE_ACTIVITY_JSON}

RUN_OR_LIST_ARG_RESPONSE = {"activity": EXAMPLE_ACTIVITY_JSON_OPTIONAL_ARGS}

LIST_ACTIVITIES_NULL_RESPONSE = dict(activities=[])

LIST_ACTIVITIES_POSITIVE_RESPONSE = {
    "activities": [EXAMPLE_ACTIVITY_JSON, EXAMPLE_ACTIVITY_JSON_OPTIONAL_ARGS]
}


# Tests for the ActivityAdapter


def test_json_payload_for_list_all_activities_is_empty():
    """Ensure the payload for list does not exist"""
    with requests_mock.Mocker() as mock_server:
        mock_server.get(ACTIVITIES_URI, json=LIST_ACTIVITIES_NULL_RESPONSE)

        adapter = ActivityAdapter(ACTIVITIES_URI)
        _ = adapter.list()

        last_request = mock_server.last_request

    # check that the request payload does not exist
    assert last_request.method == "GET"
    assert last_request.text is None


def test_list_activities_converts_no_activities_response():
    """
    An empty list should be returned when no activities are present
    """
    with requests_mock.Mocker() as mock_server:
        mock_server.get(ACTIVITIES_URI, json=LIST_ACTIVITIES_NULL_RESPONSE)

        adapter = ActivityAdapter(ACTIVITIES_URI)
        activities = adapter.list()

    assert not activities


def test_list_activities_converts_multiple_activities_response():
    """
    A list of ActivitySummary object should be returned when activities
    are present.
    """
    expected = [
        ActivitySummary.from_json(LIST_ACTIVITIES_POSITIVE_RESPONSE["activities"][0]),
        ActivitySummary.from_json(LIST_ACTIVITIES_POSITIVE_RESPONSE["activities"][1]),
    ]

    with requests_mock.Mocker() as mock_server:
        mock_server.get(ACTIVITIES_URI, json=LIST_ACTIVITIES_POSITIVE_RESPONSE)

        adapter = ActivityAdapter(ACTIVITIES_URI)
        activities = adapter.list()

    assert len(activities) == 2
    assert activities == expected


def test_list_activities_converts_specific_activity_response():
    """
    A list of with a single ActivitySummary object should be returned when an activity
    with the requested aid is present.
    """
    expected = ActivitySummary.from_json(RUN_OR_LIST_ACTIVITY_RESPONSE["activity"])

    with requests_mock.Mocker() as mock_server:
        mock_server.get(f"{ACTIVITIES_URI}/1", json=RUN_OR_LIST_ACTIVITY_RESPONSE)

        adapter = ActivityAdapter(ACTIVITIES_URI)
        activities = adapter.list(aid=1)

    assert len(activities) == 1
    assert activities[0] == expected


def test_list_activity_raises_exception_for_wrong_status():
    """
    An Exception should be raised if the HTTP response status is not OK
    """
    with requests_mock.Mocker() as mock_server:
        mock_server.get(
            f"{ACTIVITIES_URI}/1",
            json={"errorMessage": "some error"},
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )

        client = ActivityAdapter(ACTIVITIES_URI)
        with raises(Exception) as e:
            client.list(1)
        assert ("""{"errorMessage": "some error"}""",) == e.value.args


def test_run_sets_optional_values_when_undefined_by_user():
    """Check that the script uri is sent in the payload"""
    activity_name = "allocate"
    sb_id = "sbi-123"
    with requests_mock.Mocker() as mock_server:
        mock_server.post(
            ACTIVITIES_URI,
            json=RUN_OR_LIST_ACTIVITY_RESPONSE,
            status_code=HTTPStatus.CREATED,
        )

        adapter = ActivityAdapter(ACTIVITIES_URI)
        adapter.run(activity_name, sb_id)

        last_request = mock_server.last_request

    request_payload = last_request.json()
    assert request_payload["activity_name"] == activity_name
    assert request_payload["sbd_id"] == sb_id
    assert request_payload["script_args"] == {}
    assert not request_payload["prepare_only"]
    assert not request_payload["create_env"]


def test_run_activity_sends_override_arguments_when_defined_by_user():
    """Check that user-supplied script arguments are sent"""
    user_init_args = {"args": [3, 6, 9], "kwargs": {}}
    expected_script_args_payload = {"init": user_init_args}

    with requests_mock.Mocker() as mock_server:
        mock_server.post(
            ACTIVITIES_URI,
            json=RUN_OR_LIST_ARG_RESPONSE,
            status_code=HTTPStatus.CREATED,
        )

        # use the client to submit a CREATE request
        client = ActivityAdapter(ACTIVITIES_URI)
        client.run("allocate", "sbi-123", script_args=expected_script_args_payload)

        last_request = mock_server.last_request

    # check that the request JSON payload matches the expected values
    assert last_request.method == "POST"
    request_payload = last_request.json()
    assert "script_args" in request_payload
    assert request_payload["script_args"] == expected_script_args_payload


def test_run_activity_raises_exception_for_wrong_status():
    """
    An Exception should be raised if the HTTP response status is not CREATED
    """
    with requests_mock.Mocker() as mock_server:
        mock_server.post(
            ACTIVITIES_URI,
            json={"errorMessage": "some error"},
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )

        client = ActivityAdapter(ACTIVITIES_URI)
        with raises(Exception) as e:
            client.run("allocate", "sbi-123")
        assert ("""{"errorMessage": "some error"}""",) == e.value.args


# Tests for the ActivityUI


ACTIVITY_EMPTY_ARG_OVERRIDE = ActivitySummary(
    id=1,
    uri="http://127.0.0.1:5000/api/v1.0/activities/1",
    activity_name="allocate",
    sbd_id="sbi-123",
    procedure_id=1,
    script_args={},
    activity_states=[["REQUESTED", 1603381492.3060987]],
    prepare_only=False,
    state="REQUESTED",
)


ACTIVITY_WITH_ARG_OVERRIDE = ActivitySummary(
    id=2,
    uri="http://127.0.0.1:5000/api/v1.0/activities/2",
    activity_name="observe",
    sbd_id="sbi-123",
    procedure_id=2,
    script_args={
        "init": {"args": [], "kwargs": {"subarray_id": 1}},
        "main": {"args": [], "kwargs": {}},
    },
    activity_states=[
        ["REQUESTED", 1603381492.3060987],
        ["IDLE", 1601303225.8234567],
    ],
    prepare_only=True,
    state="IDLE",
)

REST_ADAPTER_LIST_RESPONSE = [ACTIVITY_EMPTY_ARG_OVERRIDE, ACTIVITY_WITH_ARG_OVERRIDE]


def parse_activity_list_response(resp):
    """Split the response from the REST API lines
    into columns
    """
    rest_responses = []
    lines = resp.splitlines()
    del lines[0:2]
    del lines[-1]
    for line in lines:
        elements = line.split()
        rest_response_object = {
            "id": elements[0],
            "activity_name": elements[1],
            "sbd_id": elements[2],
            "creation time": str(elements[3] + " " + elements[4]),
            "procedure_id": elements[5],
            "state": elements[6],
        }
        rest_responses.append(rest_response_object)
    return rest_responses


@mock.patch.object(ActivityAdapter, "run")
def test_activityui_run(mock_run_fn, capsys):
    mock_run_fn.return_value = ACTIVITY_EMPTY_ARG_OVERRIDE
    fire.Fire(ActivityUI("foo"), ["run", "allocate", "sbi-123", "--nolisten"])
    captured = capsys.readouterr()
    result = parse_activity_list_response(captured.out)

    assert result[0]["id"] == str(ACTIVITY_EMPTY_ARG_OVERRIDE.id)
    assert result[0]["activity_name"] == ACTIVITY_EMPTY_ARG_OVERRIDE.activity_name
    assert result[0]["sbd_id"] == ACTIVITY_EMPTY_ARG_OVERRIDE.sbd_id
    assert result[0]["procedure_id"] == str(ACTIVITY_EMPTY_ARG_OVERRIDE.procedure_id)
    assert result[0]["creation time"] == "2020-10-22 15:44:52"
    assert result[0]["state"] == "REQUESTED"


@mock.patch.object(ActivityAdapter, "run")
def test_activityui_run_with_optional_args(mock_run_fn, capsys):
    mock_run_fn.return_value = ACTIVITY_WITH_ARG_OVERRIDE
    fire.Fire(
        ActivityUI("foo"),
        [
            "run",
            "observe",
            "sbi-123",
            "--script_args={'init':{'kwargs': {'subarray_id': 1}}}",
            "--prepare_only=True",
            "--nolisten",
        ],
    )
    captured = capsys.readouterr()
    result = parse_activity_list_response(captured.out)

    assert result[0]["id"] == str(2)
    assert result[0]["activity_name"] == "observe"
    assert result[0]["sbd_id"] == "sbi-123"
    assert result[0]["procedure_id"] == str(2)
    assert result[0]["creation time"] == "2020-10-22 15:44:52"
    assert result[0]["state"] == "IDLE"


@mock.patch.object(ActivityAdapter, "run")
def test_activityui_run_error(mock_run_fn, capsys):
    mock_run_fn.side_effect = RuntimeError("Test Error")

    fire.Fire(ActivityUI("foo"), ["run", "allocate", "sbi-123", "--nolisten"])
    captured = capsys.readouterr()

    assert "The server encountered a problem: Test Error" in captured.out


@mock.patch.object(ActivityAdapter, "list")
def test_activityui_list(mock_list_fn, capsys):
    mock_list_fn.return_value = REST_ADAPTER_LIST_RESPONSE
    fire.Fire(ActivityUI("foo"), ["list"])
    captured = capsys.readouterr()
    result = parse_activity_list_response(captured.out)

    assert result[0]["id"] == str(1)
    assert result[1]["id"] == str(2)


@mock.patch.object(ActivityAdapter, "list")
def test_activityui_list_error(mock_list_fn, capsys):
    mock_list_fn.side_effect = RuntimeError("Test Error")

    fire.Fire(ActivityUI("foo"), ["list"])
    captured = capsys.readouterr()

    assert "The server encountered a problem: Test Error" in captured.out


@mock.patch.object(ActivityAdapter, "list")
def test_activity_describe(mock_list_fn, capsys):
    mock_list_fn.return_value = [ACTIVITY_EMPTY_ARG_OVERRIDE]

    fire.Fire(ActivityUI("foo"), ["describe"])
    captured = capsys.readouterr()

    assert ACTIVITY_EMPTY_ARG_OVERRIDE.activity_name in captured.out
    assert str(ACTIVITY_EMPTY_ARG_OVERRIDE.prepare_only) in captured.out
    assert "Script Arguments" not in captured.out
    mock_list_fn.assert_called_with(1)
    assert mock_list_fn.call_count == 2


@mock.patch.object(ActivityAdapter, "list")
def test_activityui_describe_with_optional_args(mock_list_fn, capsys):
    mock_list_fn.return_value = [ACTIVITY_WITH_ARG_OVERRIDE]

    fire.Fire(ActivityUI("foo"), ["describe"])
    captured = capsys.readouterr()

    assert "subarray_id" in captured.out
    assert "Script Arguments" in captured.out
    mock_list_fn.assert_called_with(2)


@mock.patch.object(ActivityAdapter, "list")
def test_activityui_describe_when_no_activities(mock_list_fn, capsys):
    mock_list_fn.return_value = []

    fire.Fire(ActivityUI("foo"), ["describe"])
    captured = capsys.readouterr()

    assert "No items to describe" in captured.out


@mock.patch.object(ActivityAdapter, "list")
def test_activityui_handles_describe_error(mock_list_fn, capsys):
    mock_list_fn.side_effect = RuntimeError("Test Error")

    fire.Fire(ActivityUI("foo"), ["describe"])
    captured = capsys.readouterr()

    assert "The server encountered a problem: Test Error" in captured.out
