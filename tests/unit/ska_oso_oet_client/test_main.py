# pylint: disable=W1510,C0116,C0302,C0103,W0212
# - W1510(subprocess-run-check) - not an issue - this is a test
"""
Unit tests for the REST client module.
"""
import os
import subprocess
from unittest import mock

import ska_oso_oet_client
from ska_oso_oet_client.main import RestClientUI


def test_resclientui_sets_serverurl():
    """
    Test the server URL is set correctly when RestClientUI is created. Priority should be,
    first any user provided URL, then OET_URL environment variable and finally a
    hardcoded default value if the previous two options are not set.
    """
    expected_default_url = "http://ska-oso-oet-rest-test:5000/ska-oso-oet/oet/api/v5"
    cli = RestClientUI()
    assert cli.activity._client.server_url == expected_default_url + "/activities"
    assert cli.procedure._client.server_url == expected_default_url + "/procedures"

    expected_env_url = "http://foo.bar"
    with mock.patch.dict(os.environ, {"OET_URL": expected_env_url}):
        cli = RestClientUI()
        assert cli.activity._client.server_url == expected_env_url + "/activities"
        assert cli.procedure._client.server_url == expected_env_url + "/procedures"

        expected_custom_url = "http://user-provided/url"
        cli = RestClientUI(expected_custom_url)
        assert cli.activity._client.server_url == expected_custom_url + "/activities"
        assert cli.procedure._client.server_url == expected_custom_url + "/procedures"


def test_restclientui_returns_error_when_not_passed_an_invalid_command():
    restclient = ska_oso_oet_client.main.__file__
    result = subprocess.run(
        ["python3", restclient, "blah"], capture_output=True, text=True
    )

    assert bool(result.stdout) is False
    assert result.stderr.count("ERROR") == 1


def test_restclientui_recognises_procedure_command():
    restclient = ska_oso_oet_client.main.__file__
    result = subprocess.run(
        ["python3", restclient, "procedure"], capture_output=True, text=True
    )

    # Help information should be printed to stdout, so it should not be empty
    assert result.stdout
    # Command should not raise an error so stderr should be empty
    assert bool(result.stderr) is False


def test_restclientui_recognises_activity_command():
    restclient = ska_oso_oet_client.main.__file__
    result = subprocess.run(
        ["python3", restclient, "activity"], capture_output=True, text=True
    )

    # Help information should be printed to stdout, so it should not be empty
    assert result.stdout
    # Command should not raise an error so stderr should be empty
    assert bool(result.stderr) is False
