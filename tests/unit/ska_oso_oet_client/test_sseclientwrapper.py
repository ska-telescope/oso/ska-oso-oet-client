from unittest import mock

import pytest
from sseclient import Event

from ska_oso_oet_client.sseclientwrapper import SSEClientWrapper

STREAM_URI = "http://localhost:5000/api/v1.0/stream"


@mock.patch("sseclient.SSEClient")
@mock.patch.object(SSEClientWrapper, "_filter_event_messages")
def test_listen_handles_error(mock_filter, mock_iterator):
    mock_iterator.return_value = [
        Event(id=1234, data='{"topic": "request.procedure.create"}')
    ]
    mock_filter.return_value = RuntimeError("Test Error")

    client = SSEClientWrapper(STREAM_URI)

    result = client.listen()
    assert "Test Error" in next(result)


@mock.patch("sseclient.SSEClient")
@mock.patch.object(SSEClientWrapper, "_filter_event_messages")
def test_listen_handles_keyboardinterrupt(mock_filter, mock_iterator):
    mock_iterator.return_value = [
        Event(id=1234, data='{"topic": "request.procedure.create"}')
    ]
    mock_filter.side_effect = KeyboardInterrupt()

    client = SSEClientWrapper(STREAM_URI)

    result = client.listen()
    # There should be no events because listen was interrupted before the event was yielded
    with pytest.raises(StopIteration):
        _ = next(result)


@mock.patch("sseclient.SSEClient")
def test_listen_gets_specific_topic(mock_iterator):
    mock_iterator.return_value = iter([
        Event(id=1234, data='{"topic": "procedure.lifecycle.created"}'),
        Event(data='{"topic": "request.procedure.list"}'),
    ])
    client = SSEClientWrapper(STREAM_URI)
    result = client.listen(topics="request.procedure.list", exclude="procedure.pool")
    # procedure.lifecycle.created should not appear as it was published on the wrong topic
    assert "- User request to list all the procedures is received" == next(result)


@mock.patch("sseclient.SSEClient")
def test_listen_handles_event_parse_error(mock_iterator):
    mock_iterator.return_value = iter([Event(id=1234, data="{'invalid json'}")])
    client = SSEClientWrapper(STREAM_URI)
    result = client.listen()
    assert "- ERROR Could not parse event: {'invalid json'}" == next(result)

    mock_iterator.return_value = iter(
        [Event(data='{"topic": "request.procedure.create"}')]
    )
    result = client.listen()
    # Result should be empty because no event matched the request.procedure.create topic
    with pytest.raises(StopIteration):
        next(result)

    # tests the case where the formatter returns a KeyError
    mock_iterator.return_value = iter([Event(data='{"topic": "subarray.fault"}')])
    result = client.listen()
    # Result should be empty because key error on topic is not displayed to user
    with pytest.raises(StopIteration):
        next(result)


@mock.patch("sseclient.SSEClient")
def test_subarray_id_is_parsed_correctly(mock_iterator):
    """
    Verify that subarray ID is parsed correctly in a 'procedure created' event.

    Fixes BTN-1767.
    """
    evt = (
        '{"msg_src": "SESWorker", "result": {"py/object":'
        ' "ska_oso_oet.procedure.application.application.ProcedureSummary", "id": 3,'
        ' "script": {"py/object": "ska_oso_oet.procedure.domain.FileSystemScript",'
        ' "script_uri": "file:///tmp/oda/hello_world.py"}, "script_args":'
        ' [{"py/object": "ska_oso_oet.procedure.application.application.ArgCapture",'
        ' "fn": "init", "fn_args": {"py/object":'
        ' "ska_oso_oet.procedure.domain.ProcedureInput", "args": {"py/tuple": []},'
        ' "kwargs": {"subarray_id": 1}}, "time": 1676895633.2987778}], "history":'
        ' {"py/object":'
        ' "ska_oso_oet.procedure.application.application.ProcedureHistory",'
        ' "process_states": [{"py/tuple": [{"py/reduce": [{"py/type":'
        ' "ska_oso_oet.procedure.domain.ProcedureState"}, {"py/tuple": [3]}]},'
        ' 1676895633.29832]}, {"py/tuple": [{"py/reduce": [{"py/type":'
        ' "ska_oso_oet.procedure.domain.ProcedureState"}, {"py/tuple": [2]}]},'
        ' 1676895633.2986314]}], "stacktrace": null}, "state": {"py/id": 9}}, "topic":'
        ' "procedure.lifecycle.created"}'
    )
    mock_iterator.return_value = iter([Event(id=1234, data=evt)])

    client = SSEClientWrapper(STREAM_URI)
    result = client.listen()
    assert "ready for execution on subarray 1" in next(result)
